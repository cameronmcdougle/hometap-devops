# Hometap DevOps

## The Assignment
Create some resources in Terraform, including use of modules.

## About this repo
The main part of the assignment is the Terraform, which is found in `./tf`. 

The Terraform code uses locals, data sources, and modules for each of the resources it creates.

One module was written by yours truly for this assignment and is found in [this separate repo](https://github.com/cameronmcdougle/terraform-aws-security-group).

More info on the Terraform code is found in a [dedicated readme](./tf/README.md).
