output "aws_subnets" {
  description = "printing out aws_subnets data source"
  value       = data.aws_subnets.default.ids
}
