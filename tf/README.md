# Terraform

## Table of Contents

## The Code
`main.tf` is organized as follows:

1. Locals

2. Security Groups

3. VPC endpoints

4. S3 buckets

5. ASG

6. Cloudwatch alarms

7. IAM

## What this code does
* Uses locals as a centralized place to set values like resource names

* Uses data sources to query AWS for info on existing resources like the VPC

* Creates an Autoscaling Group that scales instaces out if CPU utilization is above 70% and back in if it's below 30%

* Creates an S3 bucket

* Creates IAM roles for the ASG to access the S3 bucket

* Creates a VPC endpoint for S3 so traffic doesn't have to egress the VPC

* Creates a security group that allows outside traffic through ports 80, 443, and additionally port 443 using the S3 VPC endpoint

## Considerations
- Terraform region is hardcoded in the provider

- Default VPC is used, which is not a good practice and would be an essential change in a prod application

- Module versions are all pinned

## Issues
### Possible bug in Terraform
Accessing `aws_subnets` through a data source successfully outputs data in the form of a list of strings, however trying to use the data isn't working. There is a [bug](https://github.com/hashicorp/terraform-provider-aws/issues/32489) in Terraform related to this, but by all rights it's probably a data type issue.

## How to Use
### Prereqs
1. Create keypair with name `hometap-candidate-cameron-launch-template-key-pair`

### Instructions
#### SSH
The SSH user for AWS Linux 2 is `ec2-user`.

Make sure you add SSH to the security group!

### Testing
#### Stress test
You can stress test the ec2 instances by installing the stress package:
```
yum -y install stress
```
and then running it for a while:
```
stress --cpu 4 --timeout 900
```
Ideally, the ASG will scale out the instances a few times.
## Modifying the code
The following information is useful for modifying ASG parameters.
### Using custom scaling policies
#### Step Scaling Policies
These [CLI commands](https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-scaling-simple-step.html#as-scaling-steps) are immensely helpful, just apply them to [this ASG scaling_policy](https://github.com/terraform-aws-modules/terraform-aws-autoscaling/blob/master/examples/complete/main.tf#L284) in the module and make sure you add the [cloudwatch alarms](https://github.com/terraform-aws-modules/terraform-aws-autoscaling/blob/master/examples/complete/main.tf#L284).

#### Target Tracking Scaling
I didn't end up using these. Reading the documentation, I thought step scaling policies would be too complicated, but it turns out these were overly complicated for the use case. It's useful for learning, so here's the documentation.

To choose custom metrics, start [here[1]](https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-scaling-target-tracking.html#target-tracking-choose-metrics). [These[2]](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/viewing_metrics_with_cloudwatch.html) are the available metrics from CloudWatch, but you can use other metrics, such as [SQS messages | Terriform Registry for aws_autoscaling_policy[3]](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_policy) (see `create target tracking policy using metric math`). 

The [ASG module itself[4]](https://registry.terraform.io/modules/terraform-aws-modules/autoscaling/aws/2.0.0) - and [its source code[5]](https://github.com/terraform-aws-modules/terraform-aws-autoscaling) gives a [complete example[6]](https://github.com/terraform-aws-modules/terraform-aws-autoscaling) [(see also line 656)](https://github.com/terraform-aws-modules/terraform-aws-autoscaling) - calls the following [aws_autoscaling_policy[7]](https://github.com/terraform-aws-modules/terraform-aws-autoscaling/blob/master/main.tf#L940) Terraform resource, the documentation for which can be found [here[8]](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_policy). 

To make sense of it all, I found taking this [AWS documentation on example AWS CLI commands for creating custom scaling policies[9]](https://docs.aws.amazon.com/autoscaling/ec2/userguide/examples-scaling-policies.html) to be the most helpful, given that custom policies can only be made through API/SDK calls, and work backward through the links in this section to apply them to Terraform (and the module). 

Highlighting [customized_metric_specification](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/autoscaling_policy#customized_metric_specification) from the Terraform ASG policy registry page and the [corrollary page[10]](https://docs.aws.amazon.com/autoscaling/ec2/APIReference/API_CustomizedMetricSpecification.html) from AWS docs for each item's definition (and if you're as confused as I was on what they mean, see [this other page[11]](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/cloudwatch_concepts.html) for a more in-depth definition). There's also a [maths section[12]](https://docs.aws.amazon.com/autoscaling/ec2/userguide/ec2-auto-scaling-target-tracking-metric-math.html)

Worth noting that you can make [custom Cloudwatch metrics[13]](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/publishingMetrics.html).

My approach is to use `CPUUtilization`([2]) and some math to calculate when the threshold crosses 70% and 30% and have it scale out and in, respectively.
