locals {
  name                 = "hometap-candidate-cameron"
  bucket_name          = "${local.name}-bucket"
  log_bucket_name      = "${local.name}-bucket-logs"
  security_group_name  = "${local.name}-http-security-group"
  asg_name             = "${local.name}-asg"
  key_pair_name        = "${local.name}-launch-template-key-pair"
  launch_template_name = "${local.name}-launch-template"
  iam_policy_name      = "${local.name}-iam-policy"
  iam_role_name        = "${local.name}-iam-role"
  
  user_data     = filebase64("./user_data.sh")
}

###
# SG
###
# Data sources
data "aws_vpc" "default" {
  default = true
}

data "aws_security_group" "default" {
  name   = "default"
  vpc_id = data.aws_vpc.default.id
}

# SG
module "security_group" {
  source  = "github.com/cameronmcdougle/terraform-aws-security-group?ref=v0.2.2"

  name        = local.security_group_name
  description = "simple http ingress sg"
  vpc_id      = data.aws_vpc.default.id
}

###
# VPC Endpoints
###
module "endpoints" {
  source = "terraform-aws-modules/vpc/aws//modules/vpc-endpoints"
  version = "5.5.1"

  vpc_id             = data.aws_vpc.default.id
  security_group_ids = [module.security_group.id]

  endpoints = {
    s3 = {
      # interface endpoint
      service = "s3"
      tags    = { Name = "s3-vpc-endpoint" }
    },
  }

  tags = {
    Purpose = "${local.name}"
  }
}

###
# S3 bucket
###
# data sources
data "aws_caller_identity" "current" {}

# buckets
module "s3_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"
  version = "4.0.1"

  bucket = local.bucket_name
  acl    = "private"

  control_object_ownership = true
  object_ownership         = "ObjectWriter"

  versioning = {
    enabled = true
  }

  logging = {
    target_bucket = module.log_bucket.s3_bucket_id
    target_prefix = "log/"
    target_object_key_format = {
      partitioned_prefix = {
        partition_date_source = "DeliveryTime" # "EventTime"
      }
      # simple_prefix = {}
    }
  }

  lifecycle_rule = [
    {
      id      = "life"
      enabled = true

      filter = {}

      expiration = {
        days                         = 1
        expired_object_delete_marker = true
      }

      noncurrent_version_expiration = {
        newer_noncurrent_versions = 5
        days                      = 1
      }
    }
  ]
  tags = {
    Purpose = "${local.name}"
  }
}

module "log_bucket" {
  source = "terraform-aws-modules/s3-bucket/aws"
  version = "4.0.1"

  bucket        = local.log_bucket_name
  force_destroy = true

  access_log_delivery_policy_source_accounts = [data.aws_caller_identity.current.account_id]
  access_log_delivery_policy_source_buckets  = ["arn:aws:s3:::${local.bucket_name}"]
}

###
# ASG
###

# Not used but leaving here to discuss in interview (maybe)
data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.default.id]
  }
}

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["al2023-ami-2023*-x86_64"]
  }
}

module "asg" {
  source = "terraform-aws-modules/autoscaling/aws"
  version = "7.3.1"
  # Autoscaling group
  name = local.asg_name

  min_size                  = 1
  max_size                  = 3
  desired_capacity          = 1
  wait_for_capacity_timeout = 0
  health_check_type         = "EC2"
  vpc_zone_identifier       = ["subnet-01b57dd1878200c13", "subnet-0ace80a8af4515fcc"] # tolist(data.aws_subnets.default.ids)] #

  initial_lifecycle_hooks = [
    {
      name                  = "$StartupLifeCycleHook"
      default_result        = "CONTINUE"
      heartbeat_timeout     = 60
      lifecycle_transition  = "autoscaling:EC2_INSTANCE_LAUNCHING"
      notification_metadata = jsonencode({ "hello" = "world" })
    },
    {
      name                  = "TerminationLifeCycleHook"
      default_result        = "CONTINUE"
      heartbeat_timeout     = 180
      lifecycle_transition  = "autoscaling:EC2_INSTANCE_TERMINATING"
      notification_metadata = jsonencode({ "goodbye" = "world" })
    }
  ]

  instance_refresh = {
    strategy = "Rolling"
    preferences = {
      checkpoint_delay       = 600
      checkpoint_percentages = [35, 70, 100]
      instance_warmup        = 300
      min_healthy_percentage = 50
    }
    triggers = ["tag"]
  }

  # Launch template
  launch_template_name        = local.launch_template_name
  launch_template_description = "${local.name} launch template"
  update_default_version      = true
  key_name                    = local.key_pair_name

  image_id          = data.aws_ami.amazon_linux.id #"ami-0e9107ed11be76fde"
  instance_type     = "t3.micro"
  user_data         = local.user_data
  ebs_optimized     = false
  enable_monitoring = true

  # IAM role & instance profile
  create_iam_instance_profile = true
  iam_role_name               = local.iam_role_name
  iam_role_description        = local.name
  iam_role_tags = {
    Purpose = "${local.name}"
  }
  iam_role_policies = {
    iam_policy_name = module.iam_policy.arn
  }

  cpu_options = {
    core_count       = 1
    threads_per_core = 1
  }

  credit_specification = {
    cpu_credits = "standard"
  }

  instance_market_options = {
    market_type = "spot"
  }

  network_interfaces = [
    {
      delete_on_termination = true
      description           = "eth0"
      device_index          = 0
      security_groups       = [module.security_group.id]
    }
  ]

  # This will ensure imdsv2 is enabled, required, and a single hop which is aws security
  # best practices
  # See https://docs.aws.amazon.com/securityhub/la${local.name}/userguide/autoscaling-controls.html#autoscaling-4
  metadata_options = {
    http_endpoint               = "enabled"
    http_tokens                 = "required"
    http_put_response_hop_limit = 1
  }

  scaling_policies = {
    scale_out = {
      name                      = "scale-out"
      adjustment_type           = "ChangeInCapacity"
      policy_type               = "StepScaling"
      estimated_instance_warmup = 120
      step_adjustment = [
        {
          scaling_adjustment          = 1
          metric_interval_lower_bound = 0
        }
      ]
    },
    scale_in = {
      name                      = "scale-in"
      adjustment_type           = "ChangeInCapacity"
      policy_type               = "StepScaling"
      estimated_instance_warmup = 120
      step_adjustment = [
        {
          scaling_adjustment          = -1
          metric_interval_upper_bound = 0
        }
      ]
    }
  }

  tag_specifications = [
    {
      resource_type = "instance"
      tags          = { Purpose = "${local.name}" }
    },
    {
      resource_type = "volume"
      tags          = { Purpose = "${local.name}" }
    },
    {
      resource_type = "spot-instances-request"
      tags          = { Purpose = "${local.name}" }
    }
  ]

  tags = {
    Purpose = "${local.name}"
  }
}

###
# Cloudwatch Alarms
###
module "step_scaling_alarm_out" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/metric-alarm"
  version = "~> 4.3"

  alarm_name          = "${local.name}-step-scaling-out"
  alarm_description   = "Step Scaling Alarm"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = 1
  threshold           = 70
  period              = 300

  namespace   = "AWS/EC2"
  metric_name = "CPUUtilization"
  statistic   = "Average"

  alarm_actions = [module.asg.autoscaling_policy_arns["scale_out"]]
}

module "step_scaling_alarm_in" {
  source  = "terraform-aws-modules/cloudwatch/aws//modules/metric-alarm"
  version = "~> 4.3"

  alarm_name          = "${local.name}-step-scaling-in"
  alarm_description   = "Step Scaling Alarm"
  comparison_operator = "LessThanThreshold"
  evaluation_periods  = 1
  threshold           = 30
  period              = 300

  namespace   = "AWS/EC2"
  metric_name = "CPUUtilization"
  statistic   = "Average"

  alarm_actions = [module.asg.autoscaling_policy_arns["scale_in"]]
}

###
# IAM
###
# IAM policy
data "aws_iam_policy_document" "iam-policy-bucket" {
  statement {
    sid    = "VisualEditor0"
    effect = "Allow"
    resources = [
      module.s3_bucket.s3_bucket_arn,
      "${module.s3_bucket.s3_bucket_arn}/*"
    ]

    actions = [
      "s3:PutObject",
      "s3:GetObject",
      "s3:GetLifecycleConfiguration",
      "s3:GetBucketTagging",
      "s3:GetObjectRetention",
      "s3:GetObjectTagging",
      "s3:ListBucket",
      "s3:GetObjectVersionForReplication",
      "s3:GetBucketVersioning",
      "s3:GetObjectVersion",
    ]
  }
}

module "iam_policy" {
  source = "terraform-aws-modules/iam/aws//modules/iam-policy"
  version = "5.33.0"

  name        = local.iam_policy_name
  path        = "/"
  description = "IAM policy for ${local.name}"

  policy = data.aws_iam_policy_document.iam-policy-bucket.json
}
