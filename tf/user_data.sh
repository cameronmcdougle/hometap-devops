#!/bin/bash
if (( $EUID != 0 )); then
    yum -y install stress
    exit
else
    sudo yum -y install stress
fi
stress --cpu $(grep -ci processor /proc/cpuinfo) --timeout 900
